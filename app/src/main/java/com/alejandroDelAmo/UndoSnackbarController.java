package com.alejandroDelAmo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.TextView;

import java.io.Serializable;

/**
 * Created by Alex on 03/05/2015.
 */
public class UndoSnackbarController {
    private View mSnackView;
    private TextView mMessageView;
    private ViewPropertyAnimator mSnackAnimator;
    private Handler mHideHandler = new Handler();

    private UndoListener mUndoListener;

    // State objects
    private Serializable mUndoToken;
    private CharSequence mUndoMessage;

    public interface UndoListener {
        void onUndo(Serializable token);
    }

    public UndoSnackbarController(View snackBarView, UndoListener undoListener) {
        mSnackView = snackBarView;
        mSnackAnimator = mSnackView.animate();
        mUndoListener = undoListener;

        mMessageView = (TextView) mSnackView.findViewById(R.id.snackbar_message);
        mSnackView.findViewById(R.id.snackbar_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideUndoSnack(false);
                        mUndoListener.onUndo(mUndoToken);
                    }
                });

        hideUndoSnack(true);
    }

    public void showUndoSnack(boolean immediate, CharSequence message, Serializable undotoken) {
        mUndoToken = undotoken;
        mUndoMessage = message;
        mMessageView.setText(mUndoMessage);

        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable,
                mSnackView.getResources().getInteger(R.integer.undosnack_hide_delay));

        mSnackView.setVisibility(View.VISIBLE);
        if (immediate) {
            mSnackView.setAlpha(1);
        } else {
            mSnackAnimator.cancel();
            mSnackAnimator
                    .alpha(1)
                    .setDuration(
                            mSnackView.getResources()
                                    .getInteger(android.R.integer.config_shortAnimTime))
                    .setListener(null);
        }
    }

    public void hideUndoSnack(boolean immediate) {
        mHideHandler.removeCallbacks(mHideRunnable);
        if (immediate) {
            mSnackView.setVisibility(View.GONE);
            mSnackView.setAlpha(0);
            mUndoMessage = null;
            mUndoToken = null;

        } else {
            mSnackAnimator.cancel();
            mSnackAnimator
                    .alpha(0)
                    .setDuration(mSnackView.getResources()
                            .getInteger(android.R.integer.config_shortAnimTime))
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mSnackView.setVisibility(View.GONE);
                            mUndoMessage = null;
                            mUndoToken = null;
                        }
                    });
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putCharSequence("undo_message", mUndoMessage);
        outState.putSerializable("undo_token", mUndoToken);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mUndoMessage = savedInstanceState.getCharSequence("undo_message");
            mUndoToken = savedInstanceState.getSerializable("undo_token");

            if (mUndoToken != null || !TextUtils.isEmpty(mUndoMessage)) {
                showUndoSnack(true, mUndoMessage, mUndoToken);
            }
        }
    }

    private Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hideUndoSnack(false);
        }
    };
}
