package com.alejandroDelAmo.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.alejandroDelAmo.R;
import com.alejandroDelAmo.adapters.RankingListAdapter;
import com.alejandroDelAmo.provider.DBManager;

/**
 * Created by alex on 12/05/2015.
 */
public class RankingFragment extends Fragment {

    private RankingListAdapter rankingListAdapter;
    private RecyclerView rankingRecyclerView;
    private View mSnackbar;

    //private String query = "SELECT * FROM Expenses ORDER BY Date DESC";
    private String query = "SELECT * FROM Ranking";

    //private UndoSnackbarController mUndoController;

    //public static boolean refresh = false;

    public RankingFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_cards, container, false);
        ImageButton addButton = (ImageButton) root.findViewById(R.id.add_button);
        addButton.setVisibility(View.INVISIBLE);

        mSnackbar = root.findViewById(R.id.snackbar_layout);
        mSnackbar.setVisibility(View.INVISIBLE);

        rankingRecyclerView = (RecyclerView) root.findViewById(R.id.fragment_list);
        rankingListAdapter = new RankingListAdapter(getActivity().getApplicationContext(),
                DBManager.getInstance().getCursor(query));

        initRecyclerView();

        //Toast.makeText(root.getContext(),String.valueOf(DBManager.getInstance().getRanking()), Toast.LENGTH_LONG).show();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        rankingListAdapter.swapCursors(DBManager.getInstance().getCursor(query));

    }
    private void initRecyclerView() {
        rankingRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        rankingRecyclerView.setHasFixedSize(false);
        rankingRecyclerView.setAdapter(rankingListAdapter);


    }



    private void startAddEditActivity(Intent intent) {
        startActivityForResult(intent, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == Activity.RESULT_OK) {
                    rankingListAdapter.swapCursors(DBManager.getInstance()
                            .getCursor(query));
                }
        }
    }
}
