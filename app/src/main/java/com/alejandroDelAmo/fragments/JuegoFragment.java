package com.alejandroDelAmo.fragments;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.alejandroDelAmo.R;
import com.alejandroDelAmo.activities.JugarActivity;
import com.alejandroDelAmo.provider.DBManager;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by alex on 10/05/2015.
 */

public class JuegoFragment extends Fragment {


    private Button siguenteButton;
    private Spinner traduccion;
    private Spinner spinnerPrimerIdioma;
    private Spinner spinnerSegundoIdioma;
    private RadioButton primeraOpcion;
    private RadioButton segundaOpcion;
    private RadioButton tiempoOpcion;
    private RadioGroup grupoButton;
    private EditText tiempo;


    public JuegoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_jugar, container, false);

        traduccion = (Spinner) root.findViewById(R.id.spiner_traduccion);
        siguenteButton = (Button) root.findViewById(R.id.add_button);
        spinnerPrimerIdioma = (Spinner) root.findViewById(R.id.spiner_primer_idioma);
        spinnerSegundoIdioma = (Spinner) root.findViewById(R.id.spiner_segundo_idioma);
        primeraOpcion = (RadioButton) root.findViewById(R.id.radio5);
        segundaOpcion = (RadioButton) root.findViewById(R.id.radio10);
        tiempoOpcion = (RadioButton) root.findViewById(R.id.radioTiempo);
        tiempo = (EditText) root.findViewById(R.id.tiempo_maximo);
        grupoButton = (RadioGroup) root.findViewById(R.id.radiogroup);

        ArrayList<String> traducciones = DBManager.getInstance().getTraducciones();
        traducciones.add(0, "Selecciona una traduccion");

        final ArrayAdapter<String> adapterTraducciones = new ArrayAdapter<String>(root.getContext(), android.R.layout.simple_spinner_item, traducciones);
        adapterTraducciones.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        tiempoOpcion.setEnabled(false);

        traduccion.setAdapter(adapterTraducciones);
        traduccion.setSelection(0, false);

        traduccion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String[] aux = adapterTraducciones.getItem(position).toString().split("_");
                ArrayList<String> idiomas = new ArrayList<String>(Arrays.asList(aux));
                idiomas.add(0, "Selecciona un idioma");

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, idiomas);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerPrimerIdioma.setAdapter(adapter);
                spinnerSegundoIdioma.setAdapter(adapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerPrimerIdioma.setSelection(0);
        spinnerSegundoIdioma.setSelection(0);


        siguenteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((spinnerPrimerIdioma.getSelectedItemPosition() > 0) && (spinnerSegundoIdioma.getSelectedItemPosition() > 0)) {
                    if (spinnerPrimerIdioma.getSelectedItem().toString().equals(spinnerSegundoIdioma.getSelectedItem().toString())) {
                        Toast.makeText(v.getContext(), "Se tiene que selecionar 2 idiomas diferentes", Toast.LENGTH_LONG).show();
                    } else {
                        ArrayList<String> aux = DBManager.getInstance().primerIdiomaTraduccion(spinnerPrimerIdioma.getSelectedItem().toString());


                        final Intent intent = new Intent(getActivity(), JugarActivity.class);
                        intent.putExtra("Idioma", spinnerPrimerIdioma.getSelectedItem().toString());
                        intent.putExtra("Idioma2", spinnerSegundoIdioma.getSelectedItem().toString());
                        intent.putExtra("Traduccion", traduccion.getSelectedItem().toString());
                        String modo = null;
                        if (primeraOpcion.isChecked()) modo = "5 preguntas";
                        if (segundaOpcion.isChecked()) modo = "10 preguntas";
                        if (tiempoOpcion.isChecked()) {
                            modo = "tiempo";
                            intent.putExtra("Tiempo", tiempo.getText().toString());
                        }
                        intent.putExtra("Modo", modo);
                        if (modo.equalsIgnoreCase("5 preguntas") && aux.size() >= 5) startActivity(intent);
                        else if (modo.equalsIgnoreCase("5 preguntas") && aux.size() >= 10) startActivity(intent);
                        else Toast.makeText(v.getContext(), "La traduccion no tiene las suficientes palabras", Toast.LENGTH_LONG).show();


                    }
                } else
                    Toast.makeText(v.getContext(), "Se tiene que seleccionar 2 idiomas", Toast.LENGTH_LONG).show();


            }
        });




        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
