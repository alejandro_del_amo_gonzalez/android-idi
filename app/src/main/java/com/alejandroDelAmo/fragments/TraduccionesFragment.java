package com.alejandroDelAmo.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;


import com.alejandroDelAmo.R;
import com.alejandroDelAmo.RecyclerViewItemClickListener;
import com.alejandroDelAmo.SwipeDismissRecyclerViewTouchListener;
import com.alejandroDelAmo.UndoSnackbarController;
import com.alejandroDelAmo.activities.AddTraduccionActivity;
import com.alejandroDelAmo.adapters.TraduccionListAdapter;
import com.alejandroDelAmo.dialogs.TraduccionInfoDialog;
import com.alejandroDelAmo.models.Traduccion;
import com.alejandroDelAmo.provider.DBManager;

import java.io.Serializable;

/**
 *
 */
public class TraduccionesFragment extends Fragment {

    private TraduccionListAdapter mTagListAdapter;
    private RecyclerView mTagsRecyclerView;
    private View mSnackbar;

    private UndoSnackbarController mUndoController;

    public static boolean refresh = false;
    private String query = "SELECT * FROM Traducciones WHERE deleted=0";

    public TraduccionesFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_cards, container, false);
        ImageButton addButton = (ImageButton) root.findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddTraduccionActivity.class);
                intent.putExtra("Mode", "Add");
                startAddEditActivity(intent);
            }
        });
        mTagsRecyclerView = (RecyclerView) root.findViewById(R.id.fragment_list);
        mTagListAdapter = new TraduccionListAdapter(getActivity().getApplicationContext(),DBManager.getInstance().getCursor(query));

        mSnackbar = root.findViewById(R.id.snackbar_layout);

        mUndoController = new UndoSnackbarController(mSnackbar, new UndoSnackbarController.UndoListener() {
            @Override
            public void onUndo(Serializable token) {
                // Si es de tipo Traduccion

                if (token instanceof Traduccion) {
                    ContentValues aux = ((Traduccion) token).toContentValues();

                    ContentValues cv = new ContentValues();
                    cv.put("deleted", "0");
                    //DBManager db = DBManager.getInstance();
                    DBManager.getInstance().update("Traducciones",cv,"Idioma=? and Idioma2=?",
                            new String[]{aux.getAsString("Idioma"),aux.getAsString("Idioma2")});
                    mTagListAdapter.swapCursors(DBManager.getInstance().getCursor(query));
                } else {
                    mUndoController.hideUndoSnack(true);
                    Toast.makeText(getActivity(), R.string.undo_error, Toast.LENGTH_SHORT).show();
                }

            }
        });

        initRecyclerView();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
       // if (refresh)
            mTagListAdapter.swapCursors(DBManager.getInstance().getCursor(query));
        refresh = false;
        if (mUndoController != null)
            mUndoController.hideUndoSnack(true);
    }

    private void initRecyclerView() {
        mTagsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mTagsRecyclerView.setHasFixedSize(false);
        mTagsRecyclerView.setAdapter(mTagListAdapter);

        RecyclerViewItemClickListener.OnItemClickListener listener =
                new RecyclerViewItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position) {
                        v.playSoundEffect(android.view.SoundEffectConstants.CLICK);
                        final Intent intent = new Intent(getActivity(), AddTraduccionActivity.class);
                        intent.setAction("Edit");
                        intent.putExtra("Mode", "Edit");
                        Traduccion t = mTagListAdapter.getItemAt(position);
                        //intent.putExtra("ID", t.getId())
                        intent.putExtra("PrimerIdioma", t.getPrimerIdioma());
                        intent.putExtra("SegundoIdioma", t.getSegundoIdioma());
                        intent.putStringArrayListExtra("PalabrasTraduccionPrimerIdioma", t.getPalabrasPrimerIdioma());
                        intent.putStringArrayListExtra("PalabrasTraduccionSegundoIdioma", t.getPalabrasSegundoidioma());

                        TraduccionInfoDialog.Builder builder = new TraduccionInfoDialog.Builder();
                        builder.setName(t.getPrimerIdioma()+"_"+t.getSegundoIdioma());
                        builder.setPalabrasPrimer(t.getPalabrasPrimerIdioma());
                        builder.setPalabrasSegundo(t.getPalabrasSegundoidioma());
                        builder.setOnEditButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startAddEditActivity(intent);
                            }
                        });

                        builder.create().show(getFragmentManager(), "Traduccion");
                        //startAddEditActivity(intent);
                    }
                };
        SwipeDismissRecyclerViewTouchListener touchListener =
                new SwipeDismissRecyclerViewTouchListener(
                        mTagsRecyclerView,
                        new SwipeDismissRecyclerViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            @Override
                            public void onDismiss(RecyclerView recyclerView,
                                                  int[] reverseSortedPositions) {
                                DBManager db = DBManager.getInstance();

                                for (int position : reverseSortedPositions) {

                                    Traduccion t = mTagListAdapter.getItemAt(position);
                                    ContentValues cv = new ContentValues();
                                    cv.put("deleted", "1");
                                    if ((db.update("Traducciones",cv, "Idioma=? and Idioma2=?",new String[]{t.getPrimerIdioma(),t.getSegundoIdioma()})) != 0) {
                                        //db.deleteTable(i.getName().toString());
                                        mTagListAdapter.notifyItemRemoved(position);
                                        mUndoController.showUndoSnack(false,getString(R.string.del_traduccion),t);
                                    } else
                                        Toast.makeText(getActivity(),
                                                R.string.delete_traduccion_error,
                                                Toast.LENGTH_SHORT).show();
                                }
                                mTagListAdapter.swapCursors(db.getCursor(query));
                            }
                        });

        mTagsRecyclerView.setOnTouchListener(touchListener);
        mTagsRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(getActivity(),
                listener));
        mTagsRecyclerView.setOnScrollListener(touchListener.makeScrollListener());
    }

    private void startAddEditActivity(Intent intent) {
        startActivityForResult(intent, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == Activity.RESULT_OK) {
                    mTagListAdapter.swapCursors(DBManager.getInstance()
                            .getCursor(query));
                }
        }
    }

}
