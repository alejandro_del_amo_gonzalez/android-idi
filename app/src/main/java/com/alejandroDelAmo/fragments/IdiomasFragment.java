package com.alejandroDelAmo.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.alejandroDelAmo.R;
import com.alejandroDelAmo.RecyclerViewItemClickListener;
import com.alejandroDelAmo.SwipeDismissRecyclerViewTouchListener;
import com.alejandroDelAmo.UndoSnackbarController;
import com.alejandroDelAmo.activities.AddIdiomaActivity;
import com.alejandroDelAmo.adapters.IdiomaListAdapter;
import com.alejandroDelAmo.dialogs.IdiomaInfoDialog;
import com.alejandroDelAmo.models.Idioma;
import com.alejandroDelAmo.provider.DBManager;

import java.io.Serializable;


/**
 *
 * Created by Alex on 11/04/2015.
 */
public class IdiomasFragment extends Fragment {

    private IdiomaListAdapter idiomaListAdapter;
    private RecyclerView idiomaRecyclerView;
    private View mSnackbar;

    //private String query = "SELECT * FROM Expenses ORDER BY Date DESC";
    private String query = "SELECT * FROM Idiomas WHERE deleted = 0";

    private UndoSnackbarController mUndoController;

    public static boolean refresh = false;


    public IdiomasFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_cards, container, false);
        ImageButton addButton = (ImageButton) root.findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddIdiomaActivity.class);
                intent.putExtra("Mode", "Añadir");
                startAddEditActivity(intent);
            }
        });
        idiomaRecyclerView = (RecyclerView) root.findViewById(R.id.fragment_list);
        idiomaListAdapter = new IdiomaListAdapter(getActivity().getApplicationContext(),
                DBManager.getInstance().getCursor(query));

        mSnackbar = root.findViewById(R.id.snackbar_layout);


        mUndoController = new UndoSnackbarController(mSnackbar, new UndoSnackbarController.UndoListener() {
            @Override
            public void onUndo(Serializable token) {
                // Si es de tipo Idioma

                if (token instanceof Idioma) {
                    ContentValues aux = ((Idioma) token).toContentValues();

                    ContentValues cv = new ContentValues();
                    cv.put("deleted","0");
                    //DBManager db = DBManager.getInstance();
                    //db.insert("Expenses", cv);
                    DBManager.getInstance().update("Idiomas",cv,"Idioma =?",aux.getAsString("Name"));
                    idiomaListAdapter.swapCursors(DBManager.getInstance().getCursor(query));
                } else {
                    mUndoController.hideUndoSnack(true);
                    Toast.makeText(getActivity(), R.string.undo_error, Toast.LENGTH_SHORT).show();
                }

            }
        });

        initRecyclerView();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        //if (refresh)
            idiomaListAdapter.swapCursors(DBManager.getInstance().getCursor(query));
        refresh = false;
        if (mUndoController != null)
            mUndoController.hideUndoSnack(true);
    }

    private void initRecyclerView() {
        idiomaRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        idiomaRecyclerView.setHasFixedSize(false);

        idiomaRecyclerView.setAdapter(idiomaListAdapter);

        RecyclerViewItemClickListener.OnItemClickListener listener =
                new RecyclerViewItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View v, int pos) {
                        v.playSoundEffect(android.view.SoundEffectConstants.CLICK);
                        mUndoController.hideUndoSnack(true);
                        final Intent intent = new Intent(getActivity(), AddIdiomaActivity.class);
                        intent.setAction("Edit");
                        intent.putExtra("Mode", "Edit");
                        Idioma i = idiomaListAdapter.getItemAt(pos);
                        intent.putExtra("Name",i.getName());

                       // Bundle bundle = new Bundle();
                       // bundle.putStringArray("arrayLabel", (String[]) i.getPalabras().toArray());
                        intent.putStringArrayListExtra("Palabras",i.getPalabras());


                        IdiomaInfoDialog.Builder builder = new IdiomaInfoDialog.Builder();
                        builder.setName(i.getName());
                        builder.setPalabras(i.getPalabras());
                        builder.setOnEditButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startAddEditActivity(intent);

                            }
                        });

                        builder.create().show(getFragmentManager(), "Idiomas");
                    }
                };


        SwipeDismissRecyclerViewTouchListener touchListener =
                new SwipeDismissRecyclerViewTouchListener(
                        idiomaRecyclerView,
                        new SwipeDismissRecyclerViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            @Override
                            public void onDismiss(RecyclerView recyclerView,
                                                  int[] reverseSortedPositions) {



                                DBManager db = DBManager.getInstance();


                                for (int position : reverseSortedPositions) {

                                    Idioma i = idiomaListAdapter.getItemAt(position);
                                    ContentValues cv = new ContentValues();
                                    cv.put("deleted","1");
                                    if ((db.update("Idiomas",cv, "Idioma=?", i.getName().toString())) != 0) {
                                        //db.deleteTable(i.getName().toString());
                                        DBManager.getInstance().setDeletedTraduccion(i.getName().toString());
                                        idiomaListAdapter.notifyItemRemoved(position);
                                        mUndoController.showUndoSnack(false,getString(R.string.del_idioma),i);
                                    } else
                                        Toast.makeText(getActivity(),
                                                R.string.delete_idioma_error,
                                                Toast.LENGTH_SHORT).show();
                                }
                                idiomaListAdapter.swapCursors(db.getCursor(query));
                            }
                        });

        idiomaRecyclerView.setOnTouchListener(touchListener);
        idiomaRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(getActivity(),
                listener));
        idiomaRecyclerView.setOnScrollListener(touchListener.makeScrollListener());

    }

    private void startAddEditActivity(Intent intent) {
        startActivityForResult(intent, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == Activity.RESULT_OK) {
                    idiomaListAdapter.swapCursors(DBManager.getInstance()
                            .getCursor(query));
                }
        }
    }
}
