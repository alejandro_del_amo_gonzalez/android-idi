package com.alejandroDelAmo.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;


import com.alejandroDelAmo.R;
import com.alejandroDelAmo.dialogs.AyudaInfoDialog;
import com.alejandroDelAmo.provider.CustomEditText;
import com.alejandroDelAmo.provider.DBManager;

import java.util.ArrayList;

public class AddIdiomaActivity extends ToolbarActivity {
    private Button mDoneBtn;
    private EditText idiomaText;
    private ListView listaPalabras;
    private Button añadirPalabra;
    private CustomEditText editarPalabra;

    private ArrayList<String> listItems = new ArrayList<String>();
    private ArrayAdapter<String> adapter;
    private boolean edit;
    private String palabraEditar;
    private int posicion;
    private ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getToolbar().setNavigationIcon(R.drawable.abc_ic_clear_mtrl_alpha);
        getSupportActionBar().setTitle("Añadir Idioma");
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(RESULT_CANCELED);
            }
        });

        mDoneBtn = (Button) getToolbar().findViewById(R.id.toolbar_button);
        mDoneBtn.setVisibility(View.VISIBLE);
        mDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(RESULT_OK);
            }
        });

        imagen = (ImageView) findViewById(R.id.imageView);
        imagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AyudaInfoDialog.Builder builder = new AyudaInfoDialog.Builder();
                builder.create().show(getFragmentManager(), "Ayuda");
            }
        });

        idiomaText = (EditText) findViewById(R.id.edit_expense_name);
        listaPalabras = (ListView) findViewById(R.id.listview);
        editarPalabra = (CustomEditText) findViewById(R.id.edit_palabra_idioma);

        añadirPalabra = (Button) findViewById(R.id.add_palabra_idioma);


        Intent in = getIntent();
        edit = in.getStringExtra("Mode").equals("Edit");
        if (edit) {
            getToolbar().setTitle(R.string.edit_idioma);
            idiomaText.setKeyListener(null);
            idiomaText.setText(in.getStringExtra("Name"));
            listItems = in.getStringArrayListExtra("Palabras");

        } else
            getToolbar().setTitle(R.string.add_idioma);

        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);
        listaPalabras.setAdapter(adapter);


        listaPalabras.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editarPalabra.setText((String) (listaPalabras.getItemAtPosition(position)));
                palabraEditar = (String) (listaPalabras.getItemAtPosition(position));
                posicion = position;
            }
        });

        listaPalabras.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.remove(adapter.getItem(position));
                //DBManager.getInstance().delete(mExpenseNameText.getText().toString(), "ID=?", String.valueOf(posicion + 1));
                Toast.makeText(view.getContext(),"Se ha borrado la palabra del idioma y d", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        editarPalabra.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                editarPalabra.setText("");
                palabraEditar = null;
                return false;
            }
        });


        añadirPalabra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editarPalabra != null) {
                    ContentValues cv = new ContentValues();
                    cv.put("Palabra", editarPalabra.getText().toString());
                    if (palabraEditar != null) {
                        //DBManager.getInstance().update(mExpenseNameText.getText().toString(), cv, "ID=?", String.valueOf(posicion));
                        adapter.remove(palabraEditar);
                        adapter.insert(editarPalabra.getText().toString(), posicion);
                        //palabraEditar=null;
                    } else {
                        boolean repetido = false;
                        for (int i = 0; i < adapter.getCount(); ++i)
                            if (adapter.getItem(i).toString().equals(editarPalabra.getText().toString()))
                                repetido = true;
                        if (!repetido) {
                            //DBManager.getInstance().insert(mExpenseNameText.getText().toString(), cv);
                            adapter.add(editarPalabra.getText().toString());
                        }

                    }


                }
                /*
                boolean nuevaPalabra = true;
                int i;
                if(palabraEditar!= null) {
                    for (i = 0; i < listaPalabras.getCount(); ++i) {
                        if (palabraEditar.equals((String) listaPalabras.getItemAtPosition(i))) {
                            nuevaPalabra = false;
                            break;
                        }
                    }
                }

                ContentValues cv = new ContentValues();
                cv.put("Palabra", editarPalabra.getText().toString());
                if (nuevaPalabra) {
                    DBManager.getInstance().insert(mExpenseNameText.getText().toString(), cv);
                    adapter.add(editarPalabra.getText().toString());
                    //adapter.notifyDataSetChanged();
                } else {

                    //DBManager.getInstance().update(mExpenseNameText.getText().toString(), cv, "ID=?", String.valueOf(posicion));
                    adapter.remove(palabraEditar);
                    adapter.insert(editarPalabra.getText().toString(), posicion);
                }
                */
                editarPalabra.setText("");
                palabraEditar = null;
/*
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);*/


            }
        });


        setResult(RESULT_CANCELED);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_add_idioma;
    }


    private void setDoneBtnEnabled(boolean enabled) {
        mDoneBtn.setEnabled(enabled);
        if (enabled)
            mDoneBtn.setTextColor(getResources().
                    getColor(R.color.primary_text_default_material_dark));
        else
            mDoneBtn.setTextColor(getResources().
                    getColor(R.color.primary_text_disabled_material_light));
    }

    private void finish(int result) {
        try {
            if (result == RESULT_OK) {

               // ContentValues cv = ex.toContentValues();
                if (edit) {
                   /* Integer id = getIntent().getIntExtra("ID", 0);
                    cv.put("ID", id);
                    DBManager.getInstance().update("Expenses", cv, "ID=?", id.toString());*/

                    DBManager.getInstance().deleteTable(idiomaText.getText().toString());
                    String[] aux = new String [adapter.getCount()];
                    for(int i=0; i < adapter.getCount();++i) aux[i] = adapter.getItem(i);
                    DBManager.getInstance().createTableIdioma(idiomaText.getText().toString(), aux);
                    DBManager.getInstance().comprobarTraduccion(idiomaText.getText().toString(), aux);

                    //DBManager.getInstance().createTableIdioma(mExpenseNameText.getText().toString(),);
                    //DBManager.getInstance().delete("Idiomas","Idioma =?",mExpenseNameText.getText().toString());


                }
                else {
                    boolean existe = DBManager.getInstance().existeixIdioma(idiomaText.getText().toString());

                    if (!existe) {
                        //Toast.makeText(getApplicationContext(),"No existe", Toast.LENGTH_SHORT).show();
                        String[] aux = new String[adapter.getCount()];
                        for (int i = 0; i < adapter.getCount(); ++i) aux[i] = adapter.getItem(i);
                        DBManager.getInstance().createTableIdioma(idiomaText.getText().toString(), aux);
                        ContentValues cv = new ContentValues();
                        cv.put("Idioma", idiomaText.getText().toString());
                        DBManager.getInstance().insert("Idiomas", cv);

                    } else Toast.makeText(getApplicationContext(), "Ya existe un idioma con el mismo nombre", Toast.LENGTH_SHORT).show();
                }
            }
            setResult(result);
            finish();

        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }


}