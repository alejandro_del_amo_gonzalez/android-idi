package com.alejandroDelAmo.activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.alejandroDelAmo.R;
import com.alejandroDelAmo.dialogs.AyudaInfoDialog;
import com.alejandroDelAmo.fragments.IdiomasFragment;
import com.alejandroDelAmo.fragments.JuegoFragment;
import com.alejandroDelAmo.fragments.RankingFragment;
import com.alejandroDelAmo.fragments.TraduccionesFragment;
import com.alejandroDelAmo.provider.DBManager;
import com.alejandroDelAmo.utils.Files;


public class MainActivity extends ToolbarActivity {
    private DrawerLayout mDrawerLayout;
    //private RecyclerView mDrawer;
    private ListView mDrawer;
    private int mCurrentSection = -1;
    static boolean locked = true;
    private boolean first = true;
    private ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.main);
        //mDrawer = (RecyclerView) mDrawerLayout.findViewById(R.id.left_drawer);
        //mDrawer.setLayoutManager(new LinearLayoutManager(this));
        mDrawer = (ListView) mDrawerLayout.findViewById(R.id.left_drawer);

        // TODO mejorar!


        mDrawer.setAdapter(new ArrayAdapter<>(
                this,
                R.layout.drawer_section,
                R.id.drawer_section_text,
                getResources().getStringArray(R.array.drawer_entries)
        ));

        mDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {;
                selectItem(position);
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                mToolbar,
                R.string.open,
                R.string.close
        ) {
            public void onDrawerClosed(View view)
            {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
                syncState();
            }

            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                syncState();
            }
        };
        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        firstChecks();
        Log.d("hh", "here");

        imagen = (ImageView) findViewById(R.id.imageView);
        imagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AyudaInfoDialog.Builder builder = new AyudaInfoDialog.Builder();
                builder.create().show(getFragmentManager(), "Ayuda");
            }
        });





    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (first) {
            selectItem(0);
            first = false;
        }
        else
            selectItem(mCurrentSection);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    private void selectItem(int pos) {
        if (pos != mCurrentSection) {
            FragmentManager fm = getFragmentManager();
            if (pos < 4) {
                View v = mDrawer.getChildAt(mCurrentSection);
                if (v != null)
                    v.setBackgroundColor(Color.parseColor("#00000000"));
                v = mDrawer.getChildAt(pos);
                if (v != null)
                    v.setBackgroundColor(getResources().getColor(R.color.Teal500));
                mCurrentSection = pos;
            }
            switch (pos) {
                case 0:
                    fm.beginTransaction().replace(R.id.content_frame, new IdiomasFragment()).commit();
                    break;
                case 1:
                    fm.beginTransaction().replace(R.id.content_frame, new TraduccionesFragment()).commit();
                    break;
                case 2:
                    fm.beginTransaction().replace(R.id.content_frame, new JuegoFragment()).commit();
                    break;
                case 3:
                    fm.beginTransaction().replace(R.id.content_frame, new RankingFragment()).commit();
                    break;
                   // Intent intent = new Intent(this, SettingsActivity.class);
                   // startActivityForResult(intent, 2);
            }
        }
        mDrawerLayout.closeDrawer(mDrawer);
    }

    private void firstChecks() {
        // Check if DB exists
        String[] dbList = getApplicationContext().databaseList();
        boolean found = false;
        int i = 0;
        while (!found && i < dbList.length)
            found = dbList[i++].equals(Files.DB_NAME);
        if (DBManager.getInstance().getDatabase() == null) {
            SQLiteDatabase db = openOrCreateDatabase(Files.DB_NAME, 0, null);
            DBManager.getInstance().setDatabase(db);
            if (!found) {
                DBManager.getInstance().fillDefault(getResources()
                        .getColor(R.color.cardview_light_background), this);

            }

        }


    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == Activity.RESULT_CANCELED) {
                    finish();
                } else {
                    locked = false;
                }
                break;
            case 2:
                locked = false;
                break;
        }
    }
}
