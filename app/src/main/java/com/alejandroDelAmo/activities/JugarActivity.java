package com.alejandroDelAmo.activities;


import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;


import com.alejandroDelAmo.R;
import com.alejandroDelAmo.dialogs.AyudaInfoDialog;
import com.alejandroDelAmo.models.Ranking;
import com.alejandroDelAmo.provider.CustomEditText;
import com.alejandroDelAmo.provider.DBManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by alex on 10/05/2015.
 */

public class JugarActivity extends ToolbarActivity {
    private Calendar calendar;
    private Button mDoneBtn;
    private ImageView imagen;
    private EditText primerIdioma;
    private EditText segundoIdioma;
    private EditText palabraMuestra;
    private Button buttonValidar;
    private CustomEditText palabraAValidar;
    private RatingBar ratingBar,ratingBar2;
    private String traduccion;
    private ArrayList<String> palabrasPosibles = new ArrayList<>();
    private ArrayList<String> soluciones = new ArrayList<>();
    private ArrayList<String> palabrasResueltas = new ArrayList<>();
    private final Handler handler = new Handler();
    private Runnable swapImage;
    private boolean correcto;
    private boolean fin  = false;
    private TimerTask testing;
    private Timer timer = new Timer();
    private ImageView imagenInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getToolbar().setNavigationIcon(R.drawable.abc_ic_clear_mtrl_alpha);
        getSupportActionBar().setTitle("Adivina la palabra");
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(RESULT_CANCELED);
            }
        });

        imagenInfo = (ImageView) findViewById(R.id.imageView);
        imagenInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AyudaInfoDialog.Builder builder = new AyudaInfoDialog.Builder();
                builder.create().show(getFragmentManager(), "Ayuda");
            }
        });

        mDoneBtn = (Button) getToolbar().findViewById(R.id.toolbar_button);
        mDoneBtn.setVisibility(View.INVISIBLE);
        mDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(RESULT_OK);
            }
        });

        imagen = (ImageView) findViewById(R.id.image);
        ratingBar = (RatingBar)findViewById(R.id.rating);
        ratingBar2 = (RatingBar)findViewById(R.id.rating2);
        primerIdioma = (EditText)findViewById(R.id.primer_idioma);
        segundoIdioma = (EditText)findViewById(R.id.segundo_idioma);
        palabraMuestra = (EditText)findViewById(R.id.palabra_adivinar);
        palabraAValidar = (CustomEditText)findViewById(R.id.edit_palabra_idioma);
        buttonValidar = (Button)findViewById(R.id.add_palabra_idioma);

        ratingBar2.setVisibility(View.INVISIBLE);
        Intent in = getIntent();
        primerIdioma.setText(in.getStringExtra("Idioma"));
        segundoIdioma.setText(in.getStringExtra("Idioma2"));
        traduccion = in.getStringExtra("Traduccion");
        if(in.getStringExtra("Modo").equals("10 preguntas")) ratingBar2.setVisibility(View.VISIBLE);
        String[] aux = traduccion.split("_");

        //Toast.makeText(getApplicationContext(), aux[0], Toast.LENGTH_LONG).show();

        if(aux[0].equalsIgnoreCase(primerIdioma.getText().toString())) {
            palabrasPosibles = DBManager.getInstance().primerIdiomaTraduccion(traduccion);
            soluciones = DBManager.getInstance().segundoIdiomaTraduccion(traduccion);
        }
        else {
            palabrasPosibles = DBManager.getInstance().segundoIdiomaTraduccion(traduccion);
            soluciones = DBManager.getInstance().primerIdiomaTraduccion(traduccion);
        }
        palabraMuestra.setText(palabraSiguente());

        //Toast.makeText(getApplicationContext(), traduccion, Toast.LENGTH_LONG).show();


        buttonValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (soluciones.contains(palabraAValidar.getText().toString())) correcto = true;
                else correcto = false;
               // mHandler.removeCallbacks(changeImage);
               // mHandler.postDelayed(changeImage, 3000);

                palabrasResueltas.add(palabraMuestra.getText().toString());

                Toast toast = new Toast(v.getContext());
                ImageView view = new ImageView(v.getContext());
                if(correcto)view.setImageResource(R.drawable.ic_tick);
                else view.setImageResource(R.drawable.ic_cross);
                toast.setView(view);
                toast.show();

                //timer.schedule(testing, 1000);
                // Toast.makeText(v.getContext(), String.valueOf(palabrasResueltas.size()), Toast.LENGTH_LONG).show();


                if (ratingBar2.getVisibility() == View.INVISIBLE) {
                    if (palabrasResueltas.size() == 5) {
                        fin = true;
                        Toast.makeText(v.getContext(), "Finalizado el juego con una puntuacion de " + String.valueOf(ratingBar.getRating()), Toast.LENGTH_LONG).show();
                        mDoneBtn.setVisibility(View.VISIBLE);
                    } else {
                        if (correcto) ratingBar.setRating(ratingBar.getRating() + (float) 1.0);
                    }
                }
                else {
                    if (palabrasResueltas.size() == 10) {
                        Toast.makeText(v.getContext(), "Finalizado el juego con una puntuacion de " + String.valueOf(ratingBar.getRating()), Toast.LENGTH_LONG).show();
                        fin = true;
                        mDoneBtn.setVisibility(View.VISIBLE);
                    }
                    else {
                        if(correcto) {
                            if (palabrasResueltas.size() > 5) {
                                ratingBar2.setRating(ratingBar2.getRating() + (float) 1.0);
                            }
                            else ratingBar.setRating(ratingBar.getRating() + (float) 1.0);
                        }
                    }



                }
                palabraAValidar.setText("");
                if(!fin)palabraMuestra.setText(palabraSiguente());

            }




    });

        setResult(RESULT_CANCELED);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_jugar;
    }


    private void finish(int result) {
        try {
            if (result == RESULT_OK) {

                String puntuacion,modo;
                if(ratingBar2.getVisibility() == View.INVISIBLE) {
                    float aux = (ratingBar.getRating()/(float)5.0)*10;
                    puntuacion = String.valueOf(aux);
                    modo = "5 preguntas";
                }
                else {
                    puntuacion = String.valueOf(ratingBar.getRating()+ratingBar2.getRating());
                    modo = "10 preguntas";
                }

                //if(ratingBar2.getVisibility() == View.INVISIBLE) {

                Calendar c = Calendar.getInstance();
                String date = c.getTime().toString();
                Ranking r = new Ranking(puntuacion,
                            date,
                            modo);

                //}
                /*else {
                    r = new Ranking((ratingBar.getRating()+ratingBar2.getRating()),
                            calendar.getTime(),
                            "10 preguntas");

                }*/
                ContentValues cv = r.toContentValues();
                DBManager.getInstance().insert("Ranking",cv);

            }
            setResult(result);
            finish();

        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }
    private String palabraSiguente(){
        int pos = new Random().nextInt(palabrasPosibles.size());
        String result = palabrasPosibles.get(pos);
        palabrasPosibles.remove(pos);

        //while(palabrasResueltas.contains(palabrasPosibles.get(pos))){
        //    pos = r.n extInt(palabrasPosibles.size());
        //}
        return result;

    }


}