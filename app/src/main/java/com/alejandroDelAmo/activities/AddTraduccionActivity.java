package com.alejandroDelAmo.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.alejandroDelAmo.R;
import com.alejandroDelAmo.dialogs.AyudaInfoDialog;
import com.alejandroDelAmo.provider.DBManager;

import java.util.ArrayList;

public class AddTraduccionActivity extends ToolbarActivity {

    private Spinner spinnerPrimerIdioma;
    private Spinner spinnerSegundoIdioma;
    private ListView listaPrimerIdioma;
    private ListView listaSegundaIdioma;
    private AutoCompleteTextView palabraPrimerIdioma;
    private AutoCompleteTextView palabraSegundoIdioma;
    private Button mDoneBtn;
    private boolean edit;
    private ArrayList<String> palabrasPrimerIdioma = new ArrayList<String>();
    private ArrayList<String> palabrasSegundoIdioma = new ArrayList<String>();
    private ArrayAdapter<String> adapterPrimerIdioma;
    private ArrayAdapter<String> adapterSegundoIdioma;
    private Button addPalabras;


    private ArrayAdapter<String> spinnerArrayAdapter;
    private ArrayAdapter<String> spinnerArrayAdapter2;

    private ArrayAdapter<String> adapterAutoTextPrimerIdioma;
    private ArrayAdapter<String> adapterAutoTextSegundoIdioma;

    private ArrayList<String> idiomas = new ArrayList<String>();

    private boolean isLeftListEnabled = true;
    private boolean isRightListEnabled = true;

    private int posicionPrimerIdioma;
    private String selectedPrimerIdioma=null;

    private int posicionSegundoIdioma;
    private String selectedSegundoIdioma=null;
    private ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getToolbar().setNavigationIcon(R.drawable.abc_ic_clear_mtrl_alpha);
        getSupportActionBar().setTitle(R.string.add_traduccion);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(RESULT_CANCELED);
            }
        });

        imagen = (ImageView) findViewById(R.id.imageView);
        imagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AyudaInfoDialog.Builder builder = new AyudaInfoDialog.Builder();
                builder.create().show(getFragmentManager(), "Ayuda");
            }
        });

        spinnerPrimerIdioma = (Spinner) findViewById(R.id.spiner_primer_idioma);
        spinnerSegundoIdioma = (Spinner) findViewById(R.id.spiner_segundo_idioma);

        listaPrimerIdioma = (ListView) findViewById(R.id.lista_primer_idioma);
        listaSegundaIdioma = (ListView) findViewById(R.id.lista_segundo_idioma);

        palabraPrimerIdioma = (AutoCompleteTextView) findViewById(R.id.autoCompletePrimerIdioma);
        palabraSegundoIdioma = (AutoCompleteTextView) findViewById(R.id.autoCompleteSegundoIdioma);

        Button mDoneBtn = (Button) getToolbar().findViewById(R.id.toolbar_button);
        mDoneBtn.setVisibility(View.VISIBLE);
        mDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(RESULT_OK);
            }
        });

        addPalabras = (Button) findViewById(R.id.add_button);

        idiomas = DBManager.getInstance().getIdiomas();
        idiomas.add(0, "Selecciona un Idioma");

        Intent in = getIntent();
        edit = in.getStringExtra("Mode").equals("Edit");

        if (edit) {


            getToolbar().setTitle(R.string.edit_traduccion);

            ArrayList<String> nIdioma = new ArrayList<>();
            nIdioma.add(in.getStringExtra("PrimerIdioma"));
            spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, nIdioma);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerPrimerIdioma.setAdapter(spinnerArrayAdapter);

            ArrayList<String> nIdioma2 = new ArrayList<>();
            nIdioma2.add(in.getStringExtra("SegundoIdioma"));
            spinnerArrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, nIdioma2);
            spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerSegundoIdioma.setAdapter(spinnerArrayAdapter2);

            palabrasPrimerIdioma = in.getStringArrayListExtra("PalabrasTraduccionPrimerIdioma");
            palabrasSegundoIdioma = in.getStringArrayListExtra("PalabrasTraduccionSegundoIdioma");

            ArrayList<String> autoTextPrimerIdioma = DBManager.getInstance().palabrasIdioma(in.getStringExtra("PrimerIdioma"));
            ArrayList<String> autoTextSegundoIdioma = DBManager.getInstance().palabrasIdioma(in.getStringExtra("SegundoIdioma"));

            adapterAutoTextPrimerIdioma = new ArrayAdapter(this, android.R.layout.simple_list_item_1, autoTextPrimerIdioma);
            palabraPrimerIdioma.setAdapter(adapterAutoTextPrimerIdioma);

            adapterAutoTextSegundoIdioma = new ArrayAdapter(this, android.R.layout.simple_list_item_1, autoTextSegundoIdioma);
            palabraSegundoIdioma.setAdapter(adapterAutoTextSegundoIdioma);

        } else {
            getToolbar().setTitle(R.string.add_traduccion);

            spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,idiomas );
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerPrimerIdioma.setAdapter(spinnerArrayAdapter);

            spinnerArrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,idiomas );
            spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerSegundoIdioma.setAdapter(spinnerArrayAdapter2);

            palabraPrimerIdioma.setThreshold(0);
            palabraSegundoIdioma.setThreshold(0);

            spinnerPrimerIdioma.setSelection(0,false);
            spinnerSegundoIdioma.setSelection(0,false);

            spinnerPrimerIdioma.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    switch (parent.getId()) {

                        case R.id.spiner_primer_idioma:
                            if (position > 0) {
                                adapterAutoTextPrimerIdioma = new ArrayAdapter(parent.getContext(),android.R.layout.simple_list_item_1,DBManager.getInstance().palabrasIdioma(spinnerArrayAdapter.getItem(position)));
                                palabraPrimerIdioma.setAdapter(adapterAutoTextPrimerIdioma);

                                if(spinnerSegundoIdioma.getSelectedItem().equals(spinnerArrayAdapter.getItem(position))) Toast.makeText(view.getContext(),"No se puede hacer una traduccion del mismo idioma", Toast.LENGTH_LONG).show();

                                adapterPrimerIdioma.clear();
                                adapterSegundoIdioma.clear();

                            }
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            spinnerSegundoIdioma.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    switch (parent.getId()) {

                        case R.id.spiner_segundo_idioma:
                            if (position > 0) {
                                adapterAutoTextSegundoIdioma = new ArrayAdapter(parent.getContext(),android.R.layout.simple_list_item_1,DBManager.getInstance().palabrasIdioma(spinnerArrayAdapter2.getItem(position)));
                                palabraSegundoIdioma.setAdapter(adapterAutoTextSegundoIdioma);

                                if(spinnerPrimerIdioma.getSelectedItem().equals(spinnerArrayAdapter2.getItem(position))) Toast.makeText(view.getContext(),"No se puede hacer una traduccion del mismo idioma", Toast.LENGTH_LONG).show();

                                adapterSegundoIdioma.clear();
                                adapterPrimerIdioma.clear();

                            }
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


        }

        addPalabras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((palabraPrimerIdioma.getText().toString().equalsIgnoreCase("")) || (palabraSegundoIdioma.getText().toString().equalsIgnoreCase(""))) {
                    Toast.makeText(v.getContext(), "Se tiene que poner 2 palabras ", Toast.LENGTH_LONG).show();
                }
                else {
                    if (adapterPrimerIdioma.getPosition(palabraPrimerIdioma.getText().toString()) ==
                            adapterSegundoIdioma.getPosition(palabraSegundoIdioma.getText().toString())
                            && (adapterPrimerIdioma.getPosition(palabraPrimerIdioma.getText().toString()) != -1))
                        Toast.makeText(v.getContext(), "Ya esta la traduccion", Toast.LENGTH_LONG).show();


                    if ((adapterAutoTextPrimerIdioma.getPosition(palabraPrimerIdioma.getText().toString()) > -1) &&
                            (adapterAutoTextSegundoIdioma.getPosition(palabraSegundoIdioma.getText().toString()) > -1) &&
                            ((adapterPrimerIdioma.getPosition(palabraPrimerIdioma.getText().toString()) != adapterSegundoIdioma.getPosition(palabraSegundoIdioma.getText().toString()))
                                    || ((adapterPrimerIdioma.getPosition(palabraPrimerIdioma.getText().toString()) == -1)
                                    && (adapterSegundoIdioma.getPosition(palabraSegundoIdioma.getText().toString()) == -1)))) {
                        adapterPrimerIdioma.add(palabraPrimerIdioma.getText().toString());
                        adapterSegundoIdioma.add(palabraSegundoIdioma.getText().toString());
                    }

                    if ((adapterAutoTextPrimerIdioma.getPosition(palabraPrimerIdioma.getText().toString()) == -1) ||
                            (adapterAutoTextSegundoIdioma.getPosition(palabraSegundoIdioma.getText().toString()) == -1))
                        Toast.makeText(v.getContext(), "No existe la palabra en el idioma", Toast.LENGTH_LONG).show();
                }
            }
        });

        listaPrimerIdioma.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                adapterPrimerIdioma.remove(adapterPrimerIdioma.getItem(position));
                adapterSegundoIdioma.remove(adapterSegundoIdioma.getItem(position));
                //DBManager.getInstance().delete(mExpenseNameText.getText().toString(), "ID=?", String.valueOf(posicion + 1));
                return false;
            }
        });

        listaSegundaIdioma.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                adapterPrimerIdioma.remove(adapterPrimerIdioma.getItem(position));
                adapterSegundoIdioma.remove(adapterSegundoIdioma.getItem(position));
                //DBManager.getInstance().delete(mExpenseNameText.getText().toString(), "ID=?", String.valueOf(posicion + 1));
                return false;
            }
        });

        adapterPrimerIdioma = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                palabrasPrimerIdioma);
        listaPrimerIdioma.setAdapter(adapterPrimerIdioma);

        adapterSegundoIdioma = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                palabrasSegundoIdioma);
        listaSegundaIdioma.setAdapter(adapterSegundoIdioma);


        listaPrimerIdioma.setOverScrollMode(ListView.OVER_SCROLL_NEVER);
        listaSegundaIdioma.setOverScrollMode(ListView.OVER_SCROLL_NEVER);

        listaPrimerIdioma.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                if (scrollState == SCROLL_STATE_TOUCH_SCROLL) {
                    isRightListEnabled = false;
                } else if (scrollState == SCROLL_STATE_IDLE) {
                    isRightListEnabled = true;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                View c = view.getChildAt(0);
                if (c != null && isLeftListEnabled) {
                    listaSegundaIdioma.setSelectionFromTop(firstVisibleItem, c.getTop());
                }
            }
        });

        listaSegundaIdioma.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_TOUCH_SCROLL) {
                    isLeftListEnabled = false;
                } else if (scrollState == SCROLL_STATE_IDLE) {
                    isLeftListEnabled = true;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                View c = view.getChildAt(0);
                if (c != null && isRightListEnabled) {
                    listaPrimerIdioma.setSelectionFromTop(firstVisibleItem, c.getTop());
                }
            }
        });


        //setDoneBtnEnabled(edit);



        setResult(RESULT_CANCELED);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_add_traduccion;
    }


    private void setDoneBtnEnabled(boolean enabled) {
        mDoneBtn.setEnabled(enabled);
        if (enabled)
            mDoneBtn.setTextColor(getResources().
                    getColor(R.color.primary_text_default_material_dark));
        else
            mDoneBtn.setTextColor(getResources().
                    getColor(R.color.primary_text_disabled_material_light));
    }

    private void finish(int result) {
        try {
            if (result == RESULT_OK) {

                if (edit) {
                    DBManager.getInstance().deleteTable(spinnerPrimerIdioma.getSelectedItem().toString()+"_"+spinnerSegundoIdioma.getSelectedItem().toString());

                    String[] aux = new String [adapterPrimerIdioma.getCount()];
                    String[] aux2 = new String [adapterSegundoIdioma.getCount()];
                    for(int i=0; i < adapterPrimerIdioma.getCount();++i) {
                        aux[i] = adapterPrimerIdioma.getItem(i);
                        aux2[i] = adapterSegundoIdioma.getItem(i);
                    }
                    DBManager.getInstance().createTableTraduccio(spinnerPrimerIdioma.getSelectedItem().toString()+"_"+spinnerSegundoIdioma.getSelectedItem().toString(),aux,aux2);

                }else {
                    boolean existe = DBManager.getInstance().existeixTraduccion(spinnerPrimerIdioma.getSelectedItem().toString(), spinnerSegundoIdioma.getSelectedItem().toString());

                    if (!existe) {
                        //Toast.makeText(getApplicationContext(),"No existe", Toast.LENGTH_SHORT).show();
                        String[] aux = new String [adapterPrimerIdioma.getCount()];
                        String[] aux2 = new String [adapterSegundoIdioma.getCount()];
                        for(int i=0; i < adapterPrimerIdioma.getCount();++i) {
                            aux[i] = adapterPrimerIdioma.getItem(i);
                            aux2[i] = adapterSegundoIdioma.getItem(i);
                        }
                        DBManager.getInstance().createTableTraduccio(spinnerPrimerIdioma.getSelectedItem().toString()+"_"+spinnerSegundoIdioma.getSelectedItem().toString(),aux,aux2);
                        ContentValues cv = new ContentValues();
                        cv.put("Idioma", spinnerPrimerIdioma.getSelectedItem().toString());
                        cv.put("Idioma2", spinnerSegundoIdioma.getSelectedItem().toString());
                        DBManager.getInstance().insert("Traducciones", cv);
                        finish();
                    } else Toast.makeText(getApplicationContext(), "Ya existe un idioma con el mismo nombre", Toast.LENGTH_SHORT).show();

                }
            }
            setResult(result);
            finish();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }
}
