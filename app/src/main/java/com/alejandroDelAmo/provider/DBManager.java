package com.alejandroDelAmo.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.alejandroDelAmo.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Interface for managing only one DB in all the application
 * Created by Alex on 03/04/2014.
 */
public class DBManager {
    private SQLiteDatabase database;

    private static DBManager singleton = new DBManager();

    public static DBManager getInstance() {
        return singleton;
    }

    private DBManager() {}

    public void setDatabase(SQLiteDatabase database) throws SQLiteException {
        if (!database.isOpen())
            throw new SQLiteException("Database is closed");
        this.database = database;
    }

    public SQLiteDatabase getDatabase() {
        return database;
    }

    public SQLiteCursor getCursor(String query, String ... args) throws SQLiteException {
        if (database == null)
            throw new SQLiteException("Database is not set");
        if (!database.isOpen())
            throw new SQLiteException("Database is closed");
        return (SQLiteCursor) database.rawQuery(query, args);
    }

    public void fillDefault(int defColor, Context context) {
        if (database == null)
            throw new SQLiteException("Database is not set");
        else if (!database.isOpen())
            throw new SQLiteException("Database is closed");

        database.execSQL("CREATE TABLE Idiomas ('ID' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,'Idioma' TEXT NOT NULL, 'deleted' INTEGER DEFAULT 0);");


        String[] idiomasDefault = context.getResources().getStringArray(R.array.default_idiomas);
        for (int i = 0; i < idiomasDefault.length; i++) {
            ContentValues idiomas = new ContentValues();
            idiomas.put("Idioma", idiomasDefault[i]);
            database.insert("Idiomas", null, idiomas);
        }

        database.execSQL("CREATE TABLE Traducciones ('ID' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,'Idioma' TEXT NOT NULL,'Idioma2' TEXT NOT NULL, 'deleted' INTEGER DEFAULT 0);");
        String[] traductorDefault = context.getResources().getStringArray(R.array.default_traductor);

            ContentValues traductor = new ContentValues();
            traductor.put("Idioma", traductorDefault[0]);
            traductor.put("Idioma2", traductorDefault[1]);
            database.insert("Traducciones", null, traductor);

        createTableIdioma(idiomasDefault[0], context.getResources().getStringArray(R.array.default_palabras_espanol));

        createTableIdioma(idiomasDefault[1], context.getResources().getStringArray(R.array.default_palabras_catalan));

        createTableTraduccio(idiomasDefault[0]+"_"+idiomasDefault[1],context.getResources().getStringArray(R.array.default_palabras_espanol),context.getResources().getStringArray(R.array.default_palabras_catalan));


        database.execSQL("CREATE TABLE Ranking ('ID' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,'Puntuacion' TEXT NOT NULL,'Modo' TEXT NOT NULL,'TiempoCreacion' TEXT NOT NULL);");


    }

    public boolean insert(String table, ContentValues values) {
        if (database == null)
            throw new SQLiteException("Database is not set");
        else if (!database.isOpen())
            throw new SQLiteException("Database is closed");
        return database.insert(table, null, values) != -1;
    }

    public int delete(String table, String where, String ... args) {
        if (database == null)
            throw new SQLiteException("Database is not set");
        else if (!database.isOpen())
            throw new SQLiteException("Database is closed");
        return database.delete(table, where, args);
    }

    public int update(String table, ContentValues values, String where, String ... args) {
        if (database == null)
            throw new SQLiteException("Database is not set");
        else if (!database.isOpen())
            throw new SQLiteException("Database is closed");
        return database.update(table, values, where, args);
    }

    public void deleteTable(String table) {
        if (database == null)
            throw new SQLiteException("Database is not set");
        else if (!database.isOpen())
            throw new SQLiteException("Database is closed");
        database.execSQL("DROP TABLE IF EXISTS "+table);
    }

    public void createTableIdioma(String table, String[] values) {
        if (database == null)
            throw new SQLiteException("Database is not set");
        else if (!database.isOpen())
            throw new SQLiteException("Database is closed");

        database.execSQL("CREATE TABLE "+table+" ('ID' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,'Palabra' TEXT NOT NULL);");

        for(int i=0; i < values.length; ++i) {
            ContentValues cv = new ContentValues();
            cv.put("Palabra",values[i]);
            database.insert(table,null,cv);
        }
    }
    public void createTableTraduccio(String table, String[] palabrasPrimer, String[]palabrasSegundo){
        if (database == null)
            throw new SQLiteException("Database is not set");
        else if (!database.isOpen())
            throw new SQLiteException("Database is closed");

        database.execSQL("CREATE TABLE "+table+" ('ID' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,'Palabra' TEXT,'Palabra2' TEXT );");

        int mida = 0;
        if(palabrasPrimer.length > palabrasSegundo.length) mida = palabrasPrimer.length;
        else mida=palabrasSegundo.length;

        for(int i=0; i < mida; ++i) {
            ContentValues cv = new ContentValues();
            cv.put("Palabra",palabrasPrimer[i]);
            cv.put("Palabra2",palabrasSegundo[i]);
            database.insert(table,null,cv);
        }

    }

    public String nameTableTraduccio(String nombre, String nombre2) {

        Cursor c = database.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                String aux = c.getString( c.getColumnIndex("name"));
                if(aux.contains(nombre) || aux.contains(nombre2)) return aux;
                c.moveToNext();
            }
        }
        return null;


    }
    public ArrayList<String> primerIdiomaTraduccion(String name) {
        Cursor c = database.rawQuery("SELECT * FROM "+name,null);
        ArrayList<String> result = new ArrayList<String> ();
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                result.add(c.getString( c.getColumnIndex("Palabra")));
                c.moveToNext();
            }
        }
        return result;
    }

    public ArrayList<String> segundoIdiomaTraduccion(String name) {
        Cursor c = database.rawQuery("SELECT * FROM "+name,null);
        ArrayList<String> result = new ArrayList<String> ();
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                result.add(c.getString( c.getColumnIndex("Palabra2")));
                c.moveToNext();
            }
        }
        return result;
    }




    public ArrayList<String> palabrasIdioma(String name) {

        Cursor c = database.rawQuery("SELECT * FROM "+name,null);
       ArrayList<String> result = new ArrayList<String> ();
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                result.add(c.getString( c.getColumnIndex("Palabra")));
                c.moveToNext();
            }
        }
        return result;
    }

    public boolean existeixIdioma(String name) {
        Cursor c = database.rawQuery("SELECT * FROM Idiomas WHERE deleted = ? AND Idioma = ?",new String[]{"0",name});
        if(c!=null && c.getCount()>0) return true;
        return false;
    }

    public boolean existeixTraduccion(String idioma,String idioma2) {
        Cursor c = database.rawQuery("SELECT * FROM Traducciones WHERE deleted = ? AND Idioma = ? AND Idioma2 = ?",new String[]{"0",idioma,idioma2});
        if(c!=null && c.getCount()>0) return true;
        c = database.rawQuery("SELECT * FROM Traducciones WHERE deleted = ? AND Idioma2 = ? AND Idioma = ?",new String[]{"0",idioma,idioma2});
        if(c!=null && c.getCount()>0) return true;
        return false;
    }

    public ArrayList<String> getIdiomas() {

        Cursor c = database.rawQuery("SELECT * FROM Idiomas WHERE deleted=0",null);
        ArrayList<String> result = new ArrayList<String> ();
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                result.add(c.getString( c.getColumnIndex("Idioma")));
                c.moveToNext();
            }
        }
        return result;
    }
    public ArrayList<String> getTraducciones() {

        Cursor c = database.rawQuery("SELECT * FROM Traducciones WHERE deleted=0",null);
        ArrayList<String> result = new ArrayList<String> ();
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                result.add(c.getString( c.getColumnIndex("Idioma"))+"_"+ c.getString(c.getColumnIndex("Idioma2")));
                c.moveToNext();
            }
        }
        return result;
    }

    public int getRanking() {
        Cursor c = database.rawQuery("SELECT * FROM Ranking",null);
        return c.getCount();
    }


    public void setDeletedTraduccion(String name) {
        Cursor c = database.rawQuery("SELECT * FROM Traducciones WHERE deleted=0",null);
        ContentValues cv = new ContentValues();
        cv.put("deleted", "1");
        //ArrayList<Traduccion> result= new ArrayList<>();
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                if(name.equalsIgnoreCase(c.getString( c.getColumnIndex("Idioma"))) || name.equalsIgnoreCase(c.getString(c.getColumnIndex("Idioma2")))) {
                    database.update("Traducciones", cv, "Idioma=? and Idioma2=?", new String[]{c.getString(c.getColumnIndex("Idioma")), c.getString(c.getColumnIndex("Idioma2"))});
                   // Traduccion t = Traduccion.fromCursor(c);
                   // result.add(t);
                }
                c.moveToNext();
            }
        }
        //return result;
    }

    public void comprobarTraduccion(String name, String[] aux) {
        ArrayList<String> soluciones = new ArrayList<String>(Arrays.asList(aux));
        Cursor c = database.rawQuery("SELECT * FROM Traducciones WHERE deleted=0",null);
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                if(name.equalsIgnoreCase(c.getString( c.getColumnIndex("Idioma")))) {
                    String traduccion = c.getString( c.getColumnIndex("Idioma"))+"_"+c.getString( c.getColumnIndex("Idioma2"));
                    Cursor d = database.rawQuery("SELECT * FROM "+traduccion,null);
                    if (d.moveToFirst()) {
                        while ( !d.isAfterLast() ) {
                            if(!soluciones.contains(d.getString(d.getColumnIndex("Palabra"))))
                                database.delete(traduccion,"Palabra =?",new String[]{d.getString(d.getColumnIndex("Palabra"))});

                            d.moveToNext();
                        }
                    }
                }
                if(name.equalsIgnoreCase(c.getString( c.getColumnIndex("Idioma2")))) {
                    String traduccion = c.getString( c.getColumnIndex("Idioma"))+"_"+c.getString( c.getColumnIndex("Idioma2"));
                    Cursor d = database.rawQuery("SELECT * FROM "+traduccion,null);
                    if (d.moveToFirst()) {
                        while ( !d.isAfterLast() ) {
                            if (!soluciones.contains(d.getString(d.getColumnIndex("Palabra2"))))
                                database.delete(traduccion, "Palabra2 =?", new String[]{d.getString(d.getColumnIndex("Palabra2"))});

                            d.moveToNext();
                        }
                    }
                }
                // Traduccion t = Traduccion.fromCursor(c);
                // result.add(t);

                c.moveToNext();
            }
        }
    }
}
