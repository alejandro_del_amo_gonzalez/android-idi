package com.alejandroDelAmo.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.alejandroDelAmo.provider.DBManager;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by alex on 01/05/2015.
 */
public class Idioma implements Serializable {

        private int mId;
        private String mName;
        private ArrayList<String> palabras;

        public static final long serialVersionUID = 30L;

        //private static SimpleDateFormat sdf = new SimpleDateFormat("d-M-y");

        public static Idioma fromCursor(Cursor c) {
            String name = c.getString(1);
            ArrayList<String> conjuntoPalabra = DBManager.getInstance().palabrasIdioma(name);


            return new Idioma(name,conjuntoPalabra);
        }


        public Idioma(String name,ArrayList<String> conjuntoPalabras) {
            mName=name;
            palabras= conjuntoPalabras;
        }

        public String getName() {
            return mName;
        }

        public void setName(String mName) {
            this.mName = mName;
        }
         public ArrayList<String> getPalabras () {
            return palabras;
        }

        public void setPalabras(ArrayList<String> conjuntoPalabras) {

            this.palabras = conjuntoPalabras;
        }


        public ContentValues toContentValues() {
            ContentValues cv = new ContentValues();

            cv.put("Name", mName);
            return cv;
        }



}
