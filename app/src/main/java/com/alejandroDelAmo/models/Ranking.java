package com.alejandroDelAmo.models;

import android.content.ContentValues;
import android.database.Cursor;


import java.io.Serializable;
import java.util.Date;

/**
 * Created by alex on 01/05/2015.
 */
public class Ranking implements Serializable {

    private int mId;
    private String puntuacion;
    private String tiempoCreacion;
    private Date tiempoHecho;
    private String modo;




    public static final long serialVersionUID = 50L;

    //private static SimpleDateFormat sdf = new SimpleDateFormat("d-M-y");

    public static Ranking fromCursor(Cursor c) {
        String puntuacion = c.getString(1);

        String modo = c.getString(2);

        String tiempoCreacion = c.getString(3);

       // Long tiempoHechoLong = c.getLong(c.getColumnIndex("TiempoHecho"));
       // Date tiempoHecho = new Date(tiempoHechoLong);


        return new Ranking(puntuacion,tiempoCreacion/*,tiempoHecho*/,modo);
    }


    public Ranking(String puntuacion, String tiempoCreacion/*,Date tiempoHecho*/, String modo) {
        this.puntuacion = puntuacion;
        this.tiempoCreacion = tiempoCreacion;
        //this.tiempoHecho = tiempoHecho;
        this.modo = modo;
    }

    public String getModo() {
        return modo;

    }
    public String getTiempoCreacion() {
        return tiempoCreacion;

    }
    public String getPuntuacion() {
        return puntuacion;

    }



    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();

        cv.put("Puntuacion", puntuacion);
        cv.put("Modo", modo);
        cv.put("TiempoCreacion", tiempoCreacion);
       // cv.put("TiemporHecho", tiempoHecho.getTime());

        return cv;
    }



}
