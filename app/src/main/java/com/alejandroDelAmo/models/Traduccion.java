package com.alejandroDelAmo.models;

import android.content.ContentValues;
import android.database.Cursor;


import com.alejandroDelAmo.provider.DBManager;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by alex on 03/05/2015.
 */
public class Traduccion  implements Serializable {
    private String primerIdioma;
    private String segundoIdioma;
    private ArrayList<String> palabrasPrimerIdioma;
    private ArrayList<String> palabrasSegundoidioma;


    public static final long serialVersionUID = 50L;

    //private static SimpleDateFormat sdf = new SimpleDateFormat("d-M-y");

    public static Traduccion fromCursor(Cursor c) {
        String primerIdioma = c.getString(1);
        String segundoIdioma = c.getString(2);
        ArrayList<String> palabrasPrimerIdioma = DBManager.getInstance().primerIdiomaTraduccion(primerIdioma + "_" + segundoIdioma);
        ArrayList<String> palabrasSegundoidioma = DBManager.getInstance().segundoIdiomaTraduccion(primerIdioma + "_" + segundoIdioma);

        return new Traduccion(primerIdioma,segundoIdioma,palabrasPrimerIdioma,palabrasSegundoidioma);
    }

    /* private Idioma(String name) {
         setName(name);
     }
*/
    public Traduccion(String primerIdioma,String segundoIdioma,ArrayList<String> palabrasPrimerIdioma,ArrayList<String> palabrasSegundoidioma) {
        this.primerIdioma=primerIdioma;
        this.segundoIdioma=segundoIdioma;
        this.palabrasPrimerIdioma=palabrasPrimerIdioma;
        this.palabrasSegundoidioma=palabrasSegundoidioma;
    }
/*
        public int getId() {
            return mId;
        }

    /*public void setId(int mId) {
        this.mId = mId;
    }*/

    public String getPrimerIdioma() {
        return primerIdioma;
    }

    public String getSegundoIdioma() {
        return segundoIdioma;
    }

    public void setPrimerIdioma(String name) {
        primerIdioma=name;
    }

    public void setSegundoIdioma(String name) {
        segundoIdioma=name;
    }

    public ArrayList<String> getPalabrasPrimerIdioma () {
        return palabrasPrimerIdioma;
    }

    public void setPalabrasPrimerIdioma(ArrayList<String> conjuntoPalabras) {

        this.palabrasPrimerIdioma = conjuntoPalabras;
    }

    public ArrayList<String> getPalabrasSegundoidioma () {
        return palabrasSegundoidioma;
    }

    public void setPalabrasSegundoidioma(ArrayList<String> conjuntoPalabras) {

        this.palabrasSegundoidioma = conjuntoPalabras;
    }
    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        // cv.put("ID", mId);
        cv.put("Idioma", primerIdioma);
        cv.put("Idioma2", segundoIdioma);
            /*cv.put
                    ("Palabras", palabras);
            //cv.put("Date", sdf.format(mDate));
            cv.put("Date", mDate.getTime());
            cv.put("Amount", mAmount);
            if (mTag != null)
                cv.put("Tag", mTag.getId());*/
        return cv;
    }
}
