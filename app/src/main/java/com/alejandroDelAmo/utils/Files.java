package com.alejandroDelAmo.utils;

import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Some utilities for file handling
 */
public class Files {
    public static final String DB_NAME = "traductor.db";

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public static File getDocumentStorageDir(String documentName) {
        // Get the directory for the user's public pictures directory.
        File file;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOCUMENTS), documentName);
        else
            file = new File(Environment.getExternalStorageDirectory(), "Documents" + documentName);
        if (!file.mkdirs()) {
            Log.e("Settings", "Directory not created");
        }
        return file;
    }


    public static void copyFile(File source, File dest) throws IOException{
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            if (is != null)
                is.close();
            if (os != null)
                os.close();
        }
    }
}
