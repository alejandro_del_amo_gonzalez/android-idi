package com.alejandroDelAmo.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alejandroDelAmo.R;
import com.alejandroDelAmo.models.Traduccion;

/**
 * Adapter for translate lists
 * Created by Alex on 11/04/2015.
 */
public class TraduccionListAdapter extends RecyclerView.Adapter<TraduccionListAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private Cursor traducciones;
    //private TagList mTagList = TagList.getInstance();

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public TextView traduccionName;
        public ViewHolder(CardView v) {
            super(v);
            mCardView = v;
            traduccionName = (TextView) v.findViewById(R.id.traduccion_name);
        }
    }

    public TraduccionListAdapter(Context c, Cursor traduccionesCursor) {
        mInflater = LayoutInflater.from(c);
        traducciones = traduccionesCursor;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.traduccion_layout, parent, false);
        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) v.getLayoutParams();
        lp.bottomMargin = 0;
        v.setLayoutParams(lp);
        return new ViewHolder((CardView) v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Traduccion t = getItemAt(position);
        if (t != null) {
            holder.traduccionName.setText(t.getPrimerIdioma()+"_"+t.getSegundoIdioma());
            RecyclerView.LayoutParams lp =
                    (RecyclerView.LayoutParams) holder.mCardView.getLayoutParams();
            if (position == getItemCount()-1)
                lp.bottomMargin = mInflater.getContext().getResources()
                        .getDimensionPixelSize(R.dimen.card_margin);
            else
                lp.bottomMargin = 0;
            holder.mCardView.setLayoutParams(lp);
        }
    }

    @Override
    public int getItemCount() {
        if (traducciones != null)
            return traducciones.getCount();
        return 0;
    }
    public void swapCursors(Cursor newCursor) {
        traducciones.close();
        traducciones = newCursor;
        notifyDataSetChanged();
    }

    public Traduccion getItemAt(int position) {
        if (traducciones.moveToPosition(position))
            return Traduccion.fromCursor(traducciones);
        return null;
    }
}
