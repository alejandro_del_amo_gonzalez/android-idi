package com.alejandroDelAmo.adapters;


import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alejandroDelAmo.R;
import com.alejandroDelAmo.models.Idioma;

/**
 *
 * Created by Alex on 11/05/2015.
 */
public class IdiomaListAdapter extends RecyclerView.Adapter<IdiomaListAdapter.ViewHolder> {

    //private ArrayList<Tag> mTags;
    private Cursor Idiomas;
    private LayoutInflater mInflater;


    /*
    private NumberFormat nf = NumberFormat.getCurrencyInstance();
    private DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
    private DateFormat tf = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder*/
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView mCardView;
        public TextView mExpenseName;
        public ViewHolder(CardView v) {
            super(v);
            mCardView = v;
            mExpenseName = (TextView) v.findViewById(R.id.idioma_name);

        }
    }

    public IdiomaListAdapter(Context c, Cursor expenseCursor) {
        mInflater = LayoutInflater.from(c);
        Idiomas = expenseCursor;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.idioma_layout, parent, false);
        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) v.getLayoutParams();
        lp.bottomMargin = 0;
        v.setLayoutParams(lp);
        return new ViewHolder((CardView) v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Idioma e = getItemAt(position);
        if (e != null) {
            holder.mExpenseName.setText(e.getName());
            RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams)
                    holder.mCardView.getLayoutParams();
            if (position == getItemCount()-1)
                lp.bottomMargin = mInflater.getContext().getResources().getDimensionPixelSize(R.dimen.card_margin);
            else
                lp.bottomMargin = 0;
            holder.mCardView.setLayoutParams(lp);
        }
    }

    @Override
    public int getItemCount() {
        if (Idiomas != null)
            return Idiomas.getCount();
        return 0;
    }

    public void swapCursors(Cursor newCursor) {
        Idiomas.close();
        Idiomas = newCursor;
        notifyDataSetChanged();
    }

    public Idioma getItemAt(int position) {
        if (Idiomas.moveToPosition(position))
            return Idioma.fromCursor(Idiomas);
        return null;
    }

    public Cursor getCurso() {
        return Idiomas;
    }

}
