package com.alejandroDelAmo.adapters;


import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.alejandroDelAmo.R;
import com.alejandroDelAmo.models.Ranking;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Locale;


/**
 * Created by alex on 10/05/2015.
 */
public class RankingListAdapter extends RecyclerView.Adapter<RankingListAdapter.ViewHolder> {

    //private ArrayList<Tag> mTags;
    private Cursor ranking;
    private LayoutInflater mInflater;


    private NumberFormat nf = NumberFormat.getCurrencyInstance();
    private DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
    private DateFormat tf = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView mCardView;
        public TextView modoRanking;
        public TextView puntuacionRanking;
        public TextView dateRanking;
        public ViewHolder(CardView v) {
            super(v);
            mCardView = v;
            modoRanking = (TextView) v.findViewById(R.id.ranking_modo);
            puntuacionRanking = (TextView) v.findViewById(R.id.ranking_puntuacion);
            dateRanking = (TextView) v.findViewById(R.id.ranking_date);
        }
    }

    public RankingListAdapter(Context c, Cursor expenseCursor) {
        mInflater = LayoutInflater.from(c);
        ranking = expenseCursor;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ranking_layout, parent, false);
        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) v.getLayoutParams();
        lp.bottomMargin = 0;
        v.setLayoutParams(lp);
        return new ViewHolder((CardView) v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Ranking r = getItemAt(position);
        if (r != null) {
            holder.modoRanking.setText(r.getModo());
            holder.puntuacionRanking.setText(r.getPuntuacion());
            holder.dateRanking.setText(r.getTiempoCreacion());
            RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams)
                    holder.mCardView.getLayoutParams();
            if (position == getItemCount()-1)
                lp.bottomMargin = mInflater.getContext().getResources().getDimensionPixelSize(R.dimen.card_margin);
            else
                lp.bottomMargin = 0;
            holder.mCardView.setLayoutParams(lp);
        }
    }

    @Override
    public int getItemCount() {
        if (ranking != null)
            return ranking.getCount();
        return 0;
    }

    public void swapCursors(Cursor newCursor) {
        ranking.close();
        ranking = newCursor;
        notifyDataSetChanged();
    }

    public Ranking getItemAt(int position) {
        if (ranking.moveToPosition(position))
            return Ranking.fromCursor(ranking);
        return null;
    }

    public Cursor getCurso() {
        return ranking;
    }

}