package com.alejandroDelAmo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.alejandroDelAmo.R;

/**
 * Created by alex on 13/05/2015.
 */
public class AyudaInfoDialog extends DialogFragment {

    private Button.OnClickListener mEditListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //AlertDialog.Builder adb = new AlertDialog.Builder(getActivity(),
        //       android.support.v7.appcompat.R.style.Theme_AppCompat_Dialog);
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View inflated = inflater.inflate(R.layout.ayuda_info_floating, null);




        /*View scroll = inflated.findViewById(R.id.desc_scroll);
        scroll.measure(0, 0);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) scroll.getLayoutParams();
        int h = scroll.getMeasuredHeight();
        if (getDp(h) < 200) {
            params.height = h;
            params.bottomMargin = getPixels(8);
        }
        scroll.setLayoutParams(params);*/
        adb.setView(inflated);
        return adb.create();

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);


    }



    public static class Builder {

        public AyudaInfoDialog create() {
            AyudaInfoDialog d = new AyudaInfoDialog();

            return d;
        }
    }
}
