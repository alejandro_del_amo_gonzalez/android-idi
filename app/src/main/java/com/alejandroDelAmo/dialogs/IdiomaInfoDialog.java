package com.alejandroDelAmo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.alejandroDelAmo.R;

import java.util.ArrayList;

/**
 * Created by Alex on 16/4/2015.
 */
public class IdiomaInfoDialog extends DialogFragment {

    private Button.OnClickListener mEditListener;
    private String mName;
    private ArrayList<String> listaPalabras;

    private ListView listViewPalabras;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //AlertDialog.Builder adb = new AlertDialog.Builder(getActivity(),
        //       android.support.v7.appcompat.R.style.Theme_AppCompat_Dialog);
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View inflated = inflater.inflate(R.layout.idioma_info_floating, null);
        Button mEditButton = (Button) inflated.findViewById(R.id.edit_button);
        mEditButton.setOnClickListener(mEditListener);

        TextView name = (TextView) inflated.findViewById(R.id.expense_name);
        name.setText(mName);

        listViewPalabras = (ListView)inflated.findViewById(R.id.idioma_palabras);



        /*View scroll = inflated.findViewById(R.id.desc_scroll);
        scroll.measure(0, 0);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) scroll.getLayoutParams();
        int h = scroll.getMeasuredHeight();
        if (getDp(h) < 200) {
            params.height = h;
            params.bottomMargin = getPixels(8);
        }
        scroll.setLayoutParams(params);*/
        adb.setView(inflated);
        return adb.create();

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, listaPalabras);

        listViewPalabras.setAdapter(adapter);


    }


    private float getDp(int pixels) {
        float d = getActivity().getResources().getDisplayMetrics().density;
        return pixels / d;
    }

    private int getPixels(int dp) {
        Float d = getActivity().getResources().getDisplayMetrics().density;
        return dp * d.intValue();
    }

    public void setOnEditButtonClickListener(final Button.OnClickListener listener) {
        mEditListener = new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                listener.onClick(v);
            }
        };
    }

    public static class Builder {
        private Button.OnClickListener mEditListener;
        private String mName;
        private ArrayList<String> listaPalabras = new ArrayList<String>();

        public void setName(String name) {
            mName = name;
        }

        public void setPalabras(ArrayList<String> palabras) {
            this.listaPalabras=palabras;
        }

        public void setOnEditButtonClickListener(final Button.OnClickListener listener) {
            mEditListener = listener;
        }

        public IdiomaInfoDialog create() {
            IdiomaInfoDialog d = new IdiomaInfoDialog();
            d.mName = mName;
            d.listaPalabras = listaPalabras;
            d.setOnEditButtonClickListener(mEditListener);
            return d;
        }
    }
}
