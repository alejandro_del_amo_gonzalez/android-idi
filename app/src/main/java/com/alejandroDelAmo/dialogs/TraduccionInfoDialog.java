package com.alejandroDelAmo.dialogs;

import android.widget.AbsListView.OnScrollListener;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.alejandroDelAmo.R;

import java.util.ArrayList;

/**
 * Created by Alex on 04/4/2015.
 */
public class TraduccionInfoDialog extends DialogFragment {

    private Button.OnClickListener mEditListener;
    private String mName;
    private ArrayList<String>palabras1;
    private ArrayList<String>palabras2;
    private ListView lista;
    private ListView lista2;
    boolean isLeftListEnabled = true;
    boolean isRightListEnabled = true;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View inflated = inflater.inflate(R.layout.traduccion_info_floating, null);
        Button mEditButton = (Button) inflated.findViewById(R.id.edit_button);
        mEditButton.setOnClickListener(mEditListener);

        TextView name = (TextView) inflated.findViewById(R.id.tag_name);
        name.setText(mName);

        lista = (ListView) inflated.findViewById(R.id.lstPrev);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, palabras1);

        lista.setAdapter(adapter);
       // lista.setLayoutParams(

        lista2 = (ListView) inflated.findViewById(R.id.lstCur);

        //ArrayList<String> aux = new ArrayList<String>();
        //aux.add("hola");
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, palabras2);

        lista2.setAdapter(adapter2);


        lista.setOverScrollMode(ListView.OVER_SCROLL_NEVER);
        lista2.setOverScrollMode(ListView.OVER_SCROLL_NEVER);

        lista.setOnScrollListener(new OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // onScroll will be called and there will be an infinite loop.
                // That's why i set a boolean value
                if (scrollState == SCROLL_STATE_TOUCH_SCROLL) {
                    isRightListEnabled = false;
                } else if (scrollState == SCROLL_STATE_IDLE) {
                    isRightListEnabled = true;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                View c = view.getChildAt(0);
                if (c != null && isLeftListEnabled) {
                    lista2.setSelectionFromTop(firstVisibleItem, c.getTop());
                }
            }
        });

        lista2.setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_TOUCH_SCROLL) {
                    isLeftListEnabled = false;
                } else if (scrollState == SCROLL_STATE_IDLE) {
                    isLeftListEnabled = true;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                View c = view.getChildAt(0);
                if (c != null && isRightListEnabled) {
                    lista.setSelectionFromTop(firstVisibleItem, c.getTop());
                }
            }
        });





        Display d = getActivity().getWindowManager().getDefaultDisplay();
        Point p = new Point();
        d.getSize(p);

        /*View scroll = inflated.findViewById(R.id.desc_scroll);
        scroll.measure(p.y, p.x);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) scroll.getLayoutParams();
        int h = scroll.getMeasuredHeight();
        int w = scroll.getMeasuredWidth();

        desc.measure(p.y,p.x);
        //h = desc.getMeasuredHeight();
        //desc.measure(h, w);
        if (getDp(h) < 200) {
            Double size = Math.ceil(mDescription.length() * getSpPixels(16) / w);
            Log.d("size", String.valueOf(h));
            Log.d("size", String.valueOf(mDescription.length() * h));
            Log.d("size", String.valueOf(mDescription.length() * h / w));
            Log.d("size", String.valueOf(size));
            params.height = size.intValue() * h;
            params.bottomMargin = getPixels(8);
        }
        scroll.setLayoutParams(params);*/
        adb.setView(inflated);
        return adb.create();
    }


    private float getDp(int pixels) {
        float d = getActivity().getResources().getDisplayMetrics().density;
        return pixels / d;
    }

    private int getPixels(int dp) {
        Float d = getActivity().getResources().getDisplayMetrics().density;
        return dp * d.intValue();
    }

    private int getSpPixels(int sp) {
        Float d = getActivity().getResources().getDisplayMetrics().scaledDensity;
        return sp * d.intValue();
    }

    public void setOnEditButtonClickListener(final Button.OnClickListener listener) {
        mEditListener = new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                listener.onClick(v);
            }
        };
    }

    public static class Builder {
        private Button.OnClickListener mEditListener;
        private String mName;
        private ArrayList<String>palabras1;
        private ArrayList<String>palabras2;
        public void setName(String name) {
            mName = name;
        }

        public void setPalabrasPrimer(ArrayList<String> palabras) {
            this.palabras1 = palabras;
        }
        public void setPalabrasSegundo(ArrayList<String> palabras) {
            this.palabras2 = palabras;
        }
        public void setOnEditButtonClickListener(final Button.OnClickListener listener) {
            mEditListener = listener;
        }

        public TraduccionInfoDialog create() {
            TraduccionInfoDialog d = new TraduccionInfoDialog();
            d.mName = mName;
            d.palabras1=palabras1;
            d.palabras2=palabras2;
            d.setOnEditButtonClickListener(mEditListener);
            return d;
        }
    }
}
